import React from "react";
import { mount } from "enzyme";
import Comments from "../components/Comments";
import * as api from "../api";

beforeEach(() => {
  jest.resetModules();
});

test("render a single comment", () => {
  // add post to localstorage
  const postId = "_RabhjkfX";
  const postObject = api.createCommentObject(
    "This is my comment",
    postId,
    "Me"
  );
  api.storeCommentObject([postObject]);

  // render Posts
  const wrapper = mount(<Comments postId={postId} currentPersona="Me" />);

  // see if single-post exists
  const yesPosts = wrapper.find('div[test-data="single-comment"]');
  expect(yesPosts.exists()).toBe(true);
  expect(yesPosts.length).toBe(1);

  wrapper.unmount();
});

test("remove a comment", () => {
  // add comment to localstorage
  const postId = "_Zydsjks";
  const postObject = api.createCommentObject("My Comment", postId, "Me");
  const postObject2 = api.createCommentObject("My 2nd Comment", postId, "Me");
  api.storeCommentObject([postObject, postObject2]);

  // set currentPersona to 'Me'
  api.storeCurrentPersona("Me");

  // render Ccmments
  const wrapper = mount(<Comments currentPersona="Me" postId={postId} />);

  // see if comments exists
  const posts = wrapper.find('div[test-data="single-comment"]');
  expect(posts.exists()).toBe(true);
  expect(posts.length).toBe(2);

  // find and remove first comment
  const firstPost = posts.first();
  const firstPostButton = firstPost.find("button");
  firstPostButton.simulate("click");

  // check if comment has been removed
  const newComment = wrapper.find('div[test-data="single-comment"]');
  const newls = api.fetchAllComments();

  expect(newComment.exists()).toBe(true);
  expect(newComment.length).toBe(1);
  expect(newls.length).toBe(1);

  wrapper.unmount();
});
