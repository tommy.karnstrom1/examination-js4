import * as api from "../api";

beforeEach(() => {
  localStorage.clear();
});

afterEach(() => {
  localStorage.clear();
});

// fetchCurrentPersona
test("should get user from localStorage", () => {
  const persona = "Steffe";
  api.storeCurrentPersona(persona);
  expect(api.fetchCurrentPersona()).toMatch(persona);
});

// generateID - Yes, eftersom den inte får gå fel då den kan ställa till med mycket
test("should generate a random ID", () => {
  const id = api.generateID();
  expect(typeof id).toBe("string");
});

describe("Posts", () => {
  // createPostObject
  test("should create post object", () => {
    const title = "TITLE";
    const content = "THIS IS MY CONTENT 👏";
    const author = "NAME";

    const postObject = api.createPostObject(title, content, author);
    const amountOfKeys = Object.keys(postObject).length;

    expect(amountOfKeys).toBe(5);
    expect(postObject).toHaveProperty("title", title);
    expect(postObject).toHaveProperty("content", content);
    expect(postObject).toHaveProperty("author", author);
    expect(postObject).toHaveProperty("id");
    expect(postObject).toHaveProperty("date");
  });

  test("should return empty post object", () => {
    const postObject = api.createPostObject();
    const amountOfKeys = Object.keys(postObject).length;

    expect(amountOfKeys).toBe(5);
    expect(postObject).toHaveProperty("title", "");
    expect(postObject).toHaveProperty("content", "");
    expect(postObject).toHaveProperty("author", undefined);
    expect(postObject).toHaveProperty("id");
    expect(postObject).toHaveProperty("date");
  });

  // storePostObject
  test("should store one posts in localstorage", () => {
    const postObject = api.createPostObject(
      "TITLE",
      "THIS IS MY CONTENT 👏",
      "NAME"
    );
    api.storePostObject([postObject]);

    const fetchedPosts = api.fetchAllPosts();
    const postObject2 = api.createPostObject(
      "ANOTHER TITLE",
      "THIS IS MY OTHER CONTENT 👏",
      "MY NAME"
    );
    api.storePostObject([postObject2, ...fetchedPosts]);

    const themPosts = localStorage.getItem("posts");
    expect(JSON.parse(themPosts).length).toBe(2);
  });

  // fetchAllPosts
  test("should fetch all posts", () => {
    const postObject = api.createPostObject(
      "TITLE",
      "THIS IS MY CONTENT 👏",
      "NAME"
    );
    const postObject2 = api.createPostObject(
      "ANOTHER TITLE",
      "THIS IS MY OTHER CONTENT 👏",
      "MY NAME"
    );
    api.storePostObject([postObject2, postObject]);
    const allPosts = api.fetchAllPosts();
    expect(allPosts.length).toBe(2);
  });

  test("should fetch all posts, return empty array", () => {
    const allPosts = api.fetchAllPosts();
    expect(allPosts).toEqual([]);
  });

  // removePost
  test("should remove one post", () => {
    const postObject = api.createPostObject(
      "TITLE",
      "THIS IS MY CONTENT 👏",
      "NAME"
    );
    const postObject2 = api.createPostObject(
      "ANOTHER TITLE",
      "THIS IS MY OTHER CONTENT 👏",
      "MY NAME"
    );
    const postObject3 = api.createPostObject(
      "THE THIRD TITLE",
      "THIS IS MY THIRD CONTENT 👏",
      "YOUR NAME"
    );
    api.storePostObject([postObject2, postObject, postObject3]);

    const allPosts = api.fetchAllPosts();
    expect(allPosts.length).toBe(3);

    const secondPostID = allPosts[1].id;
    api.removePost(secondPostID);

    const allPostsAgain = api.fetchAllPosts();
    expect(allPostsAgain.length).toBe(2);
  });
});

describe("Comments", () => {
  // createCommentObject
  test("should create comment object", () => {
    const comment = "This is my comment";
    const postObjectId = "_23tjksdjk";
    const author = "Me.";

    const commentObject = api.createCommentObject(
      comment,
      postObjectId,
      author
    );
    const amountOfKeys = Object.keys(commentObject).length;

    expect(amountOfKeys).toBe(5);
    expect(commentObject).toHaveProperty("comment", comment);
    expect(commentObject).toHaveProperty("postId", postObjectId);
    expect(commentObject).toHaveProperty("author", author);
    expect(commentObject).toHaveProperty("id");
    expect(commentObject).toHaveProperty("date");
  });

  test("should create empty comment object", () => {
    const commentObject = api.createCommentObject();
    const amountOfKeys = Object.keys(commentObject).length;

    expect(amountOfKeys).toBe(5);
    expect(commentObject).toHaveProperty("comment", "");
    expect(commentObject).toHaveProperty("postId", undefined);
    expect(commentObject).toHaveProperty("author", undefined);
    expect(commentObject).toHaveProperty("id");
    expect(commentObject).toHaveProperty("date");
  });

  // storeCommentObject
  test("should add a comment to localstorage", () => {
    const commentObject = api.createCommentObject(
      "This is my comment",
      "_23tjksdjk",
      "Me."
    );
    api.storeCommentObject([commentObject]);

    const allComments = api.fetchAllComments();
    expect(allComments.length).toBe(1);
    expect(allComments[0]).toEqual(commentObject);
  });

  // fetchAllComments
  test("should fetch all comments from localstorage", () => {
    const initialComments = api.fetchAllComments();
    expect(initialComments.length).toBe(0);

    const commentObject = api.createCommentObject(
      "This is my comment",
      "_23tjksdjk",
      "Me."
    );
    const commentObject2 = api.createCommentObject(
      "This is my second comment",
      "_23tjksdjk",
      "Me."
    );
    api.storeCommentObject([commentObject, commentObject2]);

    const allComments = api.fetchAllComments();
    expect(allComments.length).toBe(2);
    expect(allComments[0]).toEqual(commentObject);
    expect(allComments[1]).toEqual(commentObject2);
  });

  // removeComment
  test("should remove a comment from localstorage", () => {
    const commentObject = api.createCommentObject(
      "This is my comment",
      "_23tjksdjk",
      "Me."
    );
    const commentObject2 = api.createCommentObject(
      "This is my second comment",
      "_23tjksdjk",
      "Me."
    );
    api.storeCommentObject([commentObject, commentObject2]);

    const allComments = api.fetchAllComments();
    expect(allComments.length).toBe(2);

    api.removeComment(commentObject.id);

    const resultComments = api.fetchAllComments();
    expect(resultComments.length).toBe(1);
    expect(resultComments[0]).toEqual(commentObject2);
    expect(resultComments[1]).toBeUndefined();
  });

  // filterComments
  test("should filter comments", () => {
    const potsId1 = "_AAAAAAAAA";
    const potsId2 = "_BBBBBBBBB";

    const commentObject = api.createCommentObject(
      "This is my comment",
      potsId1,
      "Me."
    );
    const commentObject2 = api.createCommentObject(
      "Another comment",
      potsId2,
      "Me."
    );
    const commentObject3 = api.createCommentObject(
      "This is my third comment",
      potsId1,
      "Me."
    );
    api.storeCommentObject([commentObject, commentObject2, commentObject3]);
    const myComments = api.fetchAllComments();

    const filteredWithPostId1 = api.filterComments(myComments, potsId1);
    expect(filteredWithPostId1.length).toBe(2);
    expect(filteredWithPostId1[0]).toEqual(commentObject);
    expect(filteredWithPostId1[1]).toEqual(commentObject3);

    const filteredWithPostId2 = api.filterComments(myComments, potsId2);
    expect(filteredWithPostId2.length).toBe(1);
    expect(filteredWithPostId2[0]).toEqual(commentObject2);
  });
});

describe("Personas", () => {
  // storeCurrentPersona
  test("should set person in localstorage", () => {
    const personaName = "Me.";
    api.storeCurrentPersona(personaName);
    const persona = api.fetchCurrentPersona();
    expect(persona).toBe(personaName);
  });

  // fetchCurrentPersona
  test("should fetch current persona", () => {
    const expectedNames = ["Zac", "Esmeralda", "Morgana"];
    const persona = api.fetchCurrentPersona();
    expect(expectedNames).toEqual(expect.arrayContaining([persona]));
  });
});
