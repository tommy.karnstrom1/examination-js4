import { shallow } from "enzyme";
import React from "react";
import Button from "../components/Button";

describe("Button", () => {
  test("Should have style danger", () => {
    const element = shallow(
      <Button
        onClick={() => {}}
        children={"Click me"}
        className={"test"}
        danger
      />
    );
    expect(element.find("button").hasClass("bg-red-dark")).toBe(true);
  });

  test("Should have style normal", () => {
    const element = shallow(
      <Button onClick={() => {}} children={"Click me"} className={"test"} />
    );
    expect(element.find("button").hasClass("bg-indigo-dark")).toBe(true);
  });
});
