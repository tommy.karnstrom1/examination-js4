import React from 'react';
import { render } from 'enzyme';
import TypingIndicator from '../../components/Bot/TypingIndicator';

beforeEach(() => {
    jest.resetModules();
});

test('should render TypingIndicator while typing is true', () => {
    const wrapper = render(<TypingIndicator typing={true} />);
    const typingIndicator = wrapper.find('span');
    expect(typingIndicator.length).toBe(3);
})

test('TypingIndicator should return null while typing is false', () => {
    const wrapper = render(<TypingIndicator />);
    expect(wrapper.html()).toBe(null);
})
