import React from 'react';
import { shallow, mount } from 'enzyme';
import Bot from '../../components/Bot/Bot';
import * as api from "../../api";

test('should render Bot', () => {
    const wrapper = mount(<Bot />);
    wrapper.unmount();
});

test('should render messages', () => {
    const wrapper = shallow(<Bot />);
    const messages = [
        { bot: false, message: 'Hej!' },
        { bot: true, message: 'What\'s up!' },
        { bot: false, message: 'How U Doin\'? - Wendy Williams' },
        { bot: false, message: 'What\'s cookin\'?' }
    ];
    const renderedMessages = wrapper.instance().renderMessages(messages);
    expect(renderedMessages).toHaveLength(messages.length);
});

test('should update messages state on onSubmit', () => {
    const message = 'My message';

    const wrapper = shallow(<Bot />);
    expect(wrapper.state().messages).toHaveLength(0);
    wrapper.instance().onSubmit(message);

    expect(wrapper.state('typing')).toBe(true);
    expect(wrapper.state('messages')).toHaveLength(1);
});

test('Gets a reply from bot on submit', (done) => {
    const wrapper = mount(<Bot />);
    const form = wrapper.find('form');
    const input = wrapper.find('input[name="userMessage"]');

    const mockBotResponse = { message: 'coool', bot: true };
    const myResponse = 'wowser.';
    api.botReply = jest.fn(() => Promise.resolve(mockBotResponse));

    input.simulate('change', { target: { name: 'userMessage', value: myResponse } })
    form.simulate('submit');

    expect(wrapper.find("[data-test='botMessage']")).toHaveLength(1);

    setImmediate(() => {
        wrapper.update();
        const allMessages = wrapper.find("[data-test='botMessage']");

        expect(allMessages).toHaveLength(2);
        expect(allMessages.first().text()).toEqual(myResponse);
        expect(allMessages.last().text()).toEqual(mockBotResponse.message);

        done();
        wrapper.unmount();
    });
}); 
