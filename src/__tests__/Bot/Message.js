import React from 'react';
import { render } from 'enzyme';
import Message from '../../components/Bot/Message';

it('should render message when bot is true', () => {
    const props = {
        bot: true,
        message: 'M1'
    }
    render(<Message {...props} />)
});

it('should render message when bot is false', () => {
    const props = {
        bot: false,
        message: 'M2'
    }
    render(<Message {...props} />);
});
