import React from 'react';
import { mount } from 'enzyme';
import MessageForm from '../../components/Bot/MessageForm';

test('onChange should update state', () => {
    const data = { target: { name: 'userMessage', value: 'Its' } };

    const wrapper = mount(<MessageForm onSubmit={() => {}} />);
    expect(wrapper.state().userMessage).toBe('')

    const input = wrapper.find('[name="userMessage"]');
    input.simulate('change', data);

    expect(wrapper.state().userMessage).toBe(data.target.value);
});

test('onSubmit should clear userMessage in state', () => {
    const data = { target: { name: 'userMessage', value: 'This is my message' } };

    const wrapper = mount(<MessageForm onSubmit={jest.fn()} />);
    const input = wrapper.find('[name="userMessage"]');
    input.simulate('change', data);

    expect(wrapper.state().userMessage).toBe(data.target.value);
    wrapper.find('form').simulate('submit');

    expect(wrapper.state().userMessage).toBe('');
});
