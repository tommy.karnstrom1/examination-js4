import React from 'react';
import { mount } from 'enzyme';
import SingleComment from '../components/SingleComment';

test('render a post with remove button', () => {
    const wrapper = mount(
        <SingleComment
            id='_ZrQjshas'
            author='Me'
            onClick={() => { console.log('Clicked!') }}
            currentPersona="Me"
            comment='My Content'
            date='2019-01-17 12:12:12'
        />
    );
    expect(wrapper.exists('button[data-test="button"]')).toBe(true);
    wrapper.unmount();
});

test('render a post without remove button', () => {
    const wrapper = mount(
        <SingleComment
            id='_ZrQjshas'
            author='You'
            onClick={() => { console.log('Clicked!') }}
            currentPersona="Me"
            comment='My Content'
            date='2019-01-17 12:12:12'
        />
    );
    expect(wrapper.exists('button[data-test="button"]')).toBe(false);
    wrapper.unmount();
});

test('handle button onClick ', () => {
    let answer = 1;
    const wrapper = mount(
        <SingleComment
            id='_ZrQjshas'
            author='Me'
            onClick={() => { answer = 42; }}
            currentPersona="Me"
            comment='My Content'
            date='2019-01-17 12:12:12'
        />
    );

    let button = wrapper.find('button[data-test="button"]');
    expect(button.exists()).toBe(true);

    expect(answer).toBe(1);
    button.simulate('click');
    expect(answer).toBe(42);
    wrapper.unmount();
});