import { shallow } from 'enzyme';
import React from 'react';
import AvatarSelector from '../components/AvatarSelector';

beforeEach(() => {
    jest.resetModules();
});

describe('AvatarSelector', () => {
    test('should render each name', () => {
        expect.assertions(3);
        const arrayOfAcceptableNames = ['Zac', 'Esmeralda', 'Morgana'];
        arrayOfAcceptableNames.map((name) => {
            const element = shallow(<AvatarSelector currentPersona={name} />);
            const foundImg = element.find('img');
            expect(foundImg.prop('src')).toBe(`${name.toLowerCase()}.png`);
        })
    });

    test('currentPersona is empty, use Zac as default', () => {
        const element = shallow(<AvatarSelector currentPersona='Per' />);
        const foundImg = element.find('img');
        expect(foundImg.prop('src')).toBe('zac.png');
    });
});
