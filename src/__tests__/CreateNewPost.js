import React from "react";
import { shallow } from "enzyme";
import CreateNewPost from "../components/CreateNewPost";
import * as api from "../api";

test("render", () => {
  const wrapper = shallow(
    <CreateNewPost author="Zac" updatePosts={() => {}} />
  );
  const form = wrapper.find("form");
  expect(form.exists()).toBe(true);
});

test("check state onChange", () => {
  const newTitleStateValue = "This is my state value";
  const newContentStateValue = "This is my content value";

  const wrapper = shallow(
    <CreateNewPost author="Zac" updatePosts={() => {}} />
  );

  const titleInput = wrapper.find('input[name="title"]');
  titleInput.simulate("change", {
    target: { name: "title", value: newTitleStateValue }
  });

  const contentInput = wrapper.find('textarea[name="content"]');
  contentInput.simulate("change", {
    target: { name: "content", value: newContentStateValue }
  });

  expect(wrapper.state("title")).toBe(newTitleStateValue);
  expect(wrapper.state("content")).toBe(newContentStateValue);
});

test("check onSubmit function", () => {
  const newTitleStateValue = "This is my state value";
  const newContentStateValue = "This is my content value";

  const wrapper = shallow(
    <CreateNewPost author="Zac" updatePosts={() => {}} />
  );

  // populate state
  const titleInput = wrapper.find('input[name="title"]');
  titleInput.simulate("change", {
    target: { name: "title", value: newTitleStateValue }
  });

  const contentInput = wrapper.find('textarea[name="content"]');
  contentInput.simulate("change", {
    target: { name: "content", value: newContentStateValue }
  });

  expect(wrapper.state("title")).toBe(newTitleStateValue);
  expect(wrapper.state("content")).toBe(newContentStateValue);

  // simulate onSubmit - mock prevent default
  wrapper.simulate("submit", { preventDefault: () => {} });

  // check state reset
  expect(wrapper.state("title")).toBe("");
  expect(wrapper.state("content")).toBe("");

  // check localstorage PostObject
  const local = api.fetchAllPosts();
  const amountOfKeys = Object.keys(local[0]).length;

  expect(local.length).toBe(1);
  expect(amountOfKeys).toBe(5);
  expect(local[0]).toHaveProperty("title", newTitleStateValue);
  expect(local[0]).toHaveProperty("content", newContentStateValue);
  expect(local[0]).toHaveProperty("author", "Zac");
  expect(local[0]).toHaveProperty("id");
  expect(local[0]).toHaveProperty("date");
});
