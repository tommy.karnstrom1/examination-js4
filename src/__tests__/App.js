import React from "react";
import { render, mount } from "enzyme";
import App from "../components/App";

test("renders the app", () => {
  render(<App />);
});

test("should update currentPage in state", () => {
  const app = mount(<App />);
  const button = app.find("button");

  const initialPage = app.state("currentPage");
  expect(initialPage).toBe("home");

  button.simulate("click");
  const newPage = app.state("currentPage");
  expect(newPage).toBe("bot");

  button.simulate("click");
  const newPage2 = app.state("currentPage");
  expect(newPage2).toBe("home");

  app.unmount();
});

test("should update persona in state and localstore", () => {
  const newValue = "Morgana";
  const app = mount(<App />);
  const selectEle = app.find("select");
  selectEle.simulate("change", { target: { value: newValue } });

  const ls = localStorage.getItem("currentPersona");
  const newState = app.state("currentPersona");

  expect(ls).toBe(`"${newValue}"`);
  expect(newState).toBe(newValue);

  app.unmount();
});

test('should render Bot stuff when currentPage is set to "Bot"', () => {
  const app = mount(<App />);
  const button = app.find("button");

  // set state to bot
  button.simulate("click");
  const initialPage = app.state("currentPage");
  expect(initialPage).toBe("bot");

  // check if button text is right
  expect(button.text()).toBe("Return to forum");

  // check if Bot component is rendered
  const botArea = app.find("div[data-test='bot-area']");
  expect(botArea.exists()).toBe(true);
});

test('should render Home stuff when currentPage is set to "Home"', () => {
  const app = mount(<App />);
  const button = app.find("button");

  // set state to bot
  const initialPage = app.state("currentPage");
  expect(initialPage).toBe("home");

  // check if button text is right
  expect(button.text()).toBe("Talk to a real human");

  // check if Bot component is rendered
  const botArea = app.find("div[data-test='post-area']");
  expect(botArea.exists()).toBe(true);
});
