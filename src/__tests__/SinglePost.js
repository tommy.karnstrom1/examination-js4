import React from "react";
import { mount } from "enzyme";
import SinglePost from "../components/SinglePost";

test("render a post with remove button", () => {
  const wrapper = mount(
    <SinglePost
      title="My Title"
      content="My Content"
      author="Me"
      id="_ZrQjshas"
      date="2019-01-17 12:12:12"
      currentPersona="Me"
      onClick={() => {
        console.log("Clicked!");
      }}
    />
  );
  expect(wrapper.exists('button[data-test="button"]')).toBe(true);
  wrapper.unmount();
});

test("render a post without remove button", () => {
  const wrapper = mount(
    <SinglePost
      title="My Title"
      content="My Content"
      author="You"
      id="_ZrQjshas"
      date="2019-01-17 12:12:12"
      currentPersona="Me"
      onClick={() => {
        console.log("Clicked!");
      }}
    />
  );
  expect(wrapper.exists('button[data-test="button"]')).toBe(false);
  wrapper.unmount();
});

test("handle button onClick ", () => {
  let answer = 1;
  const wrapper = mount(
    <SinglePost
      title="My Title"
      content="My Content"
      author="Me"
      id="_ZrQjshas"
      date="2019-01-17 12:12:12"
      currentPersona="Me"
      onClick={() => {
        answer = 42;
      }}
    />
  );

  let button = wrapper.find('button[data-test="button"]');
  expect(button.exists()).toBe(true);

  expect(answer).toBe(1);
  button.simulate("click");
  expect(answer).toBe(42);
  wrapper.unmount();
});
