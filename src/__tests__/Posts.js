import React from "react";
import { mount } from "enzyme";
import Posts from "../components/Posts";
import * as api from "../api";

beforeEach(() => {
  jest.resetModules();
});

test("render a single post", () => {
  // add post to localstorage
  const postObject = api.createPostObject("Title", "Content", "Me");
  api.storePostObject([postObject]);

  // render Posts
  const wrapper = mount(<Posts currentPersona="Zac" />);

  // see if single-post exists
  const yesPosts = wrapper.find('div[test-data="single-post"]');
  expect(yesPosts.exists()).toBe(true);
  expect(yesPosts.length).toBe(1);

  wrapper.unmount();
});

test("remove a post", () => {
  // add post to localstorage
  const postObject = api.createPostObject("Title", "Content", "Me");
  const postObject2 = api.createPostObject("Title", "Content", "Me");
  api.storePostObject([postObject, postObject2]);

  // set currentPersona to 'Me'
  api.storeCurrentPersona("Me");

  // render Posts
  const wrapper = mount(<Posts currentPersona="Me" />);

  // see if posts exists
  const posts = wrapper.find('div[test-data="single-post"]');
  expect(posts.exists()).toBe(true);
  expect(posts.length).toBe(2);

  // get and remove first post
  const firstPost = posts.first();
  const firstPostButton = firstPost.find("button");
  firstPostButton.simulate("click");

  // check if post has been removed
  const newPosts = wrapper.find('div[test-data="single-post"]');
  const newls = api.fetchAllPosts();

  expect(newPosts.exists()).toBe(true);
  expect(newPosts.length).toBe(1);
  expect(newls.length).toBe(1);

  wrapper.unmount();
});
