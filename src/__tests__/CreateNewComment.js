import React from "react";
import { shallow } from "enzyme";
import CreateNewComment from "../components/CreateNewComment";
import * as api from "../api";

test("render", () => {
  const postId = "_Zxuah3s";
  const wrapper = shallow(
    <CreateNewComment postId={postId} author="Zac" updateComments={() => {}} />
  );
  const form = wrapper.find("form");
  expect(form.exists()).toBe(true);
});

test("check state onChange", () => {
  const postId = "_Zxuah3s";
  const newCommentStateValue = "This is my comment value";

  const wrapper = shallow(
    <CreateNewComment postId={postId} author="Zac" updateComments={() => {}} />
  );

  const contentInput = wrapper.find('textarea[name="comment"]');
  contentInput.simulate("change", {
    target: { name: "content", value: newCommentStateValue }
  });

  expect(wrapper.state("content")).toBe(newCommentStateValue);
});

test("check onSubmit function", () => {
  const postId = "_Zxuah3s";
  const newCommentStateValue = "This is my comment value";

  const wrapper = shallow(
    <CreateNewComment postId={postId} author="Zac" updateComments={() => {}} />
  );

  // populate state
  const contentInput = wrapper.find('textarea[name="comment"]');
  contentInput.simulate("change", {
    target: { name: "comment", value: newCommentStateValue }
  });

  // simulate onSubmit - mock prevent default
  wrapper.simulate("submit", { preventDefault: () => {} });

  // check state reset
  expect(wrapper.state("comment")).toBe("");

  // check localstorage PostObject
  const local = api.fetchAllComments();
  const firstItemInLocal = local[0];
  const amountOfKeys = Object.keys(firstItemInLocal).length;

  expect(local.length).toBe(1);
  expect(amountOfKeys).toBe(5);
  expect(firstItemInLocal).toHaveProperty("comment", newCommentStateValue);
  expect(firstItemInLocal).toHaveProperty("author", "Zac");
  expect(firstItemInLocal).toHaveProperty("id");
  expect(firstItemInLocal).toHaveProperty("date");
});
