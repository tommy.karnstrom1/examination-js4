before(function() {
  cy.clearLocalStorage();
});

describe("Posts", function() {
  it("Create a new post, exists in localstorage and appear in postlist", function() {
    const title = "This is the title!";
    const content = "This is the content!";
    const author = "Morgana";

    cy.visit("/");

    // change user to 'Morgana'
    cy.get('select[data-test="select-persona"]')
      .select("Morgana")
      .should("have.value", author);

    cy.get("form[data-test='post-form'] input[name='title']")
      .type(title)
      .should("have.value", title);

    cy.get("form[data-test='post-form'] textarea[name='content']")
      .type(content)
      .should("have.value", content);

    // when you click on the submit button
    cy.get("form[data-test='post-form'] input[type='submit']")
      .click()
      .should(() => {
        // check localstorage
        const lsLatestPost = JSON.parse(localStorage.getItem("posts"))[0];
        expect(lsLatestPost.title).to.eq(title);
        expect(lsLatestPost.content).to.eq(content);
        expect(lsLatestPost.author).to.eq(author);
      });

    // have a h2 with title
    cy.get("div[data-test='post-area'] article h2")
      .first()
      .contains(title);

    // have a p with content
    cy.get("div[data-test='post-area'] article p")
      .first()
      .contains(content);
  });

  it("Remove a post created by a user", function() {
    const author = "Esmeralda";

    cy.visit("/");

    // change user to 'Morgana'
    cy.get('select[data-test="select-persona"]')
      .select(author)
      .should("have.value", author);

    // get first post, find and click on remove button
    cy.get("div[data-test='post-area'] article button")
      .first()
      .click()
      .should(() => {
        const numberOfPosts = JSON.parse(localStorage.getItem("posts")).length;
        expect(numberOfPosts).to.equal(2);
      });
  });
});
