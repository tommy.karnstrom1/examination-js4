before(function() {
  cy.clearLocalStorage();
});

describe("Comments", () => {
  it("Post a new comment", () => {
    console.log("CREATE");
    const commentText = "This is my comment";

    cy.visit("/");

    // find first post (test-data: single-post)
    // find comment section form & fill textarea
    cy.get('div[test-data="single-post"]')
      .first()
      .find("textarea")
      .type(commentText);

    // click Submit button
    cy.get('div[test-data="single-post"]')
      .first()
      .find("input[type='submit']")
      .click();

    // check post p content
    cy.get('div[test-data="single-post"]')
      .first()
      .find('div[test-data="single-comment"]')
      .first()
      .find("p")
      .first()
      .contains(commentText);
  });
  it("Remove a comment on a post", () => {
    console.log("REMOVE");
  });
});
