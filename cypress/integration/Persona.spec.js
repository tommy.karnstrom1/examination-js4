before(function() {
  cy.clearLocalStorage();
});

describe("Persona", function() {
  it("Change Current Persona", function() {
    cy.visit("/");

    cy.get('select[data-test="select-persona"]')
      .select("Morgana")
      .should("have.value", "Morgana");

    cy.get('img[data-test="avatar-selector"]').should(
      "have.attr",
      "src",
      "/static/media/morgana.45fdd648.png"
    );
  });
});
